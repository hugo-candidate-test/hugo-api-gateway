<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

use App\Traits\ApiResponser\Error;

class Handler extends ExceptionHandler
{
    use Error;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
              switch(true) {
            case $exception instanceof ConnectException:
                $message = $exception->getMessage();
                $code = $exception->getCode();
                return $this->errorResponse($message);
                break;
            case $exception instanceof RequestException:
       
                $message = $exception->getResponse()->getReasonPhrase();
                $code = $exception->getResponse()->getStatusCode();
                return $this->errorResponse($message, $code);
                break;
            case $exception instanceof ClientException:
           
                $message = $exception->getBody()->getContents();
                $code = $exception->getCode();
                return $this->errorResponse($message, $code);
                break;
            case $exception instanceof AuthenticationException:
           
                $message = 'Unauthenticated';
                $code = 401;
                return response()->json(['message' => $message], $code);
                break;
        }
        return parent::render($request, $exception);
    }
}
