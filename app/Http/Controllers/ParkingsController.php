<?php

namespace App\Http\Controllers;
use App\Car;
use App\Traits\ApiResponser\Success;
use App\Traits\ApiResponser\Error;

use App\Traits\ApiExternalResponse\Success as responseExternalSuccess;
use App\Traits\ApiExternalResponse\Error as esponseExternalError;
use Illuminate\Http\Request;
use App\Http\Resources\CarResource;
use Illuminate\Database\QueryException;

use App\Services\CarsService;
use App\Services\TicketsService;
use App\Services\PaymentService;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

use Carbon\Carbon;

class ParkingsController extends Controller
{
    use Success;
    use Error;
    use ResponseExternalSuccess;

    public $carServices;
    public $ticketServices;
    public $paymentServices;

    public function __construct(CarsService $carServices, TicketsService $ticketServices, PaymentService $paymentServices )
    {
        $this->carServices = $carServices;
        $this->ticketServices = $ticketServices;
        $this->paymentServices = $paymentServices;
    }

    public  function index()
    {
        $cars =  $this->carServices->getCars();
        return $this->successExternalResponse($cars);
    }

    public  function checkIn(Request $request)
    {
        $carParams = [
            'license_plate' => $request->license_plate,
            'type_of_car' => $request->type_of_car
        ];
   
        $car =  $this->carServices->createCar($carParams);
        $car_license = json_decode($car)->data->license_plate;
        $type_of_car = json_decode($car)->data->type_of_car;

        $ticketParams = [
            'license_plate' => $car_license,
            'type_of_car' => $type_of_car,
            'start_time' => Carbon::now(),
            'status' => 'active'
        ];
    
        $ticket =  $this->ticketServices->createTicket($ticketParams);
        $start_time = json_decode($ticket)->data->start_time;
        $data = [
            'license_plate' => $car_license,
            'type_of_car' => $type_of_car,
            'start_time' => $start_time,
        ];
        return $this->successExternalResponse($data);
    }


    public  function checkingOut(Request $request)
    {
 
        $carParams = [ 'license_plate' => $request->license_plate];
        $car =  $this->carServices->findCardByLicence($carParams);

        $ticketParams = [
                        'license_plate' =>  json_decode($car)->data->license_plate
                    ];
       
        $ticket = $this->ticketServices->findCardByLicence($ticketParams);

        $ticketParamsToUpdate = [
                        'id' =>  json_decode($ticket)->data->id
                    ];
                    
        $this->ticketServices->updateTicket($ticketParamsToUpdate);
        
        $voucherParams = [
                        'car_id' =>  json_decode($car)->data->id,
                        'type_of_car' =>  json_decode($car)->data->type_of_car,
                        'ticket' => $ticket,
                        'license_plate' =>  json_decode($car)->data->license_plate
                    ];
                    
        $this->createVoucher($voucherParams);

        return $this->successExternalResponse($ticket);
    }

     public  function createVoucher($params)
    {
       
        $ticketParams = [
                        'license_plate' =>  json_decode($params['ticket'])->data->license_plate
                    ];
    
        $ticket = $this->ticketServices->findCardByLicence($ticketParams);
      
        $voucheParams = [
                        'car_id' =>  $params['car_id'],
                        'start_time' =>  json_decode($params['ticket'])->data->start_time->date,
                        'end_time' =>  json_decode($params['ticket'])->data->end_time->date,
                        'type_of_car' => $params['type_of_car']
                    ];

        $result = $this->paymentServices->createPayment($voucheParams);
    
        return $this->successExternalResponse($ticket);
    }

    public  function subscribe(Request $request)
    {

        $carParams = [ 'license_plate' => $request->license_plate];
        $car =  $this->carServices->findCardByLicence($carParams);

        $carParamsToUpdate = [
                        'type_of_car' =>  $request->type_of_car,
                        'car_id' =>  json_decode($car)->data->id
                    ];
      
        $this->carServices->updateCar($carParamsToUpdate);
        $car =  $this->carServices->findCardByLicence($carParams);

        return $this->successExternalResponse($car);
    }

      public  function startMonth(Request $request)
    {

        $response = $this->ticketServices->startMonth();

        return $this->successExternalResponse($response);
    }

}
