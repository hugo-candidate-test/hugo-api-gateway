<?php 
namespace App\Services;

use App\Traits\ExternalServices\Request;

class PaymentService
{
    use Request;

    public $base_url;
    public $key_secret;

    public function __construct()
    {
      $this->base_url = config('services.payments.base_url');
      $this->key_secret = config('services.payments.key_secret');
    }

    public function createPayment($params)
    {
      return $this->call('POST', '/payments', $params);
    }

   
}