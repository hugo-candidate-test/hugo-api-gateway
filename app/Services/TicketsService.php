<?php 
namespace App\Services;

use App\Traits\ExternalServices\Request;

use Carbon\Carbon;

class TicketsService
{
    use Request;

    public $base_url;
    public $key_secret;

    public function __construct()
    {
        $this->base_url = config('services.tickets.base_url');
        $this->key_secret = config('services.tickets.key_secret');
    }
        
    public function createTicket($params)
    {
        return $this->call('POST', '/tickets', $params);
    }

    public function updateTicket($params)
    {
        return $this->call('PATCH', '/tickets/'. $params["id"],  ['end_time' => Carbon::now() ] );
    }

    public function findCardByLicence($params)
    {
        return $this->call('PATCH', '/ticket-find-by-licence', $params);
    }

     public function startMonth()
    {
        return $this->call('GET', '/tickets-off');
    }

}