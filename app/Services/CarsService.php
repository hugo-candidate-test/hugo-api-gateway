<?php 
namespace App\Services;

use App\Traits\ExternalServices\Request;

class CarsService
{
    use Request;

    public $base_url;
    public $key_secret;

    public function __construct()
    {
      $this->base_url = config('services.cars.base_url');
      $this->key_secret = config('services.cars.key_secret');
    }

      
    public function getCars()
    {
      return $this->call('GET', '/cars');
    }
      
    public function createCar($params)
    {
      return $this->call('POST', '/cars', $params);
    }
    
    public function findCardByLicence($params)
    {
        return $this->call('PATCH', '/cars-find-by-licence', $params);
    }

     public function updateCar($params)
    {
        return $this->call('PATCH', '/cars/'. $params['car_id'] , $params);
    }
}