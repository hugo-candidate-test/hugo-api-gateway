<?php
namespace App\Traits\ApiExternalResponse;

use Illuminate\Http\Response;

trait Error 
{
  
    public function errorResponse($message, $code)
    {
        return response()->json($message, $code)->header('Content-Type', 'Application/json');
    }


}
