<?php
namespace App\Traits\ApiExternalResponse;

use Illuminate\Http\Response;

trait Success 
{
  
    public function successExternalResponse($data, $code = Response::HTTP_OK)
    {
        return response($data, $code)->header('Content-Type', 'application/json');
    }


}
