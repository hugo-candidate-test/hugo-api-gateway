<?php
namespace App\Traits\ApiResponser;

use Illuminate\Http\Response;

trait Error 
{
  
    public function errorResponse($data, $code = Response::HTTP_UNPROCESSABLE_ENTITY, $message = 'UNPROCESSABLE ENTITY')
    {
        return response()->json(['data' => $data, 'message' => $message], $code);
    }


}
