<?php
namespace App\Traits\ApiResponser;

use Illuminate\Http\Response;

trait Success 
{
  
    public function SuccessResponse($data, $code = Response::HTTP_OK)
    {
        return response()->json(['data' => $data], $code);
    }


}
