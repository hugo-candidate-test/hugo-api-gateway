<?php
namespace App\Traits\ExternalServices;

use GuzzleHttp\Client;
use Illuminate\Http\Response;

trait Request 
{ 
    public function call($method, $request, $params = [], $headers = [])
    {
        
        $client = new Client(['base_uri' => $this->base_url]);
        $headers['Authorization'] = $this->key_secret; 
        $response = $client->request($method, $request, ['form_params' => $params, 'headers' => $headers]);
        
        return $response->getBody()->getContents();
    
    }


}
