# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


Hey !!

Esta es una breve guia para este APIGATWAY

Se encarga de la solicitudes hacia y desde otros miscroservicios que espero que puedas ver en ese grupo.

Hay una seria de cosas que tienes que instalar antes de empezar.

1.  Mongodb
2.  Composer
3.  PHP 7..
4.  phpunit


* Para corre el servidor usar **php -S localhost:port -t ./public**
* Para correr los test **vendor/bin/phpunit**
*

## Dependencia externas.

*  https://github.com/symfony/symfony/blob/20e8cb207bb1c1773497cb8ad0e75b2e782131f2/src/Symfony/Component/HttpFoundation/Response.php
*  https://laravel.com/docs/5.8/passport
*  Laravel/Lumen
*  https://github.com/guzzle/guzzle


## Vamos hacerlo.

Recuerda los pasos anteriores para que podamos seguir.


## en Vars.

He creado una clase agnostica llamada Request que se encarga de generar las solicitudes hacia los diferentes servicios que estand dentro de un folder llamado 
> /Services

Estos traits contienen todas las acciones de cada uno de los servicios que corresponden a un endpoint especifico de un micro servicio.
Esto usa un archivo de configuracion llamado
>  Config/services.php

Dentro de esto se encuentra la url principal en donde estara alojado el micro servicio en cuestion.

'tickets' => [
          'base_url' => env('TICKET_URL'),
          'key_secret' => env('KEY_TICKET')
        ]
`
Lo importante es que agregas las variables de entorno para los servicios descritos.

## en Arquitectura.

Si bien es cierto el apigateway  no cuenta con test por el momento los microservicios fueron desarrollados siguiendo un desarrollo guiado por prueba usando el ciclo;


**> RED-GREEN-REFACTOR** 

Y Usando el pricipio de **unica responsabilidad** de **SOLID**. 

## Importante.


Debes de levantar cada microservicio por separado en un puerto defirente puede hacerlo usando este comando

`php -S localhost:port -t ./public`

**NOTA: Recuerda agregar a las env var los valores de las url de cada instancia.** 

## Autotizacion.

Recuerda! estoy usando el flujo de Oauht2 clients credenciales asi que no te olvides de generar un cliente para golper los endpoint de este Gateway.

Puede hacerlo usando este comando:
`php artisan passport:client`

No olvides agregar el nombre de tu client, No necesitaremos una url callback asi que no te preocupes por generar uno. 

Puedes apoyar te la documentacio  en este link de postman https://www.getpostman.com/collections/14ad7f03d864ce6388e8

## ENDPOINTS.

*  **/oauth/token** Te ayudara para autenticarte no olvide enviarle como body grant_type => client_credentials, client_id => value , client_secret => value
*  **api/check-in**  Registra entrada
*  **/api/check-out**  Registra salida
*  **/api/subscribe ** Da de alta vehículo official o residente
*  **/api/start-month** Inicia el mes 


