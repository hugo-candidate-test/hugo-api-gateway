<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api', 'middleware' => 'client.credentials'], function () use ($router) {
    $router->get('/cars', 'ParkingsController@index');
    $router->post('/check-in', 'ParkingsController@checkIn');
    $router->patch('/check-out', 'ParkingsController@checkingOut');
    $router->post('/subscribe', 'ParkingsController@subscribe');
    $router->post('/voucher', 'ParkingsController@createVoucher');
    $router->post('/start-month', 'ParkingsController@startMonth');
});