<?php

return [
        'cars' => [
          'base_url' => env('CARS_URL'),
          'key_secret' => env('KEY_CARS')
        ],
        'tickets' => [
          'base_url' => env('TICKET_URL'),
          'key_secret' => env('KEY_TICKET')
        ],
        'payments' => [
          'base_url' => env('PAYMENT_URL'),
          'key_secret' => env('KEY_PAYMENT')
        ]
];